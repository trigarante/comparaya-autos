#! /bin/bash
pwd
#Se comprueba que exista el directorio backup dentro de la carpeta script
#En caso de que no exista, se va a crear
if [ -d script/backup ];
then
echo "Sí, sí existe backup."
else
pwd
echo "No, no existe backup, se creará."
cd script || exit
mkdir backup
ls -la
cd ..
fi
#Se indica la aseguradora a la cual se desea consultar las marcass
for var in aba afirme aig ana atlas axa banorte elaguila el_potosi general_seguros gnp hdi latino mapfre migo qualitas sura vepormas zurich; do
  if [ -f script/$var.json ];
  then
    echo "Sí, sí existe "$var", movere archivos"
    mv script/$var.json script/backup/
  else
    echo "No existe "$var" no movere archivos"
  fi

  pet="$(curl https://dev.core-comparaya.com/v1/$var/marcas)"
  if [[ $pet =~ ^\[ ]]; then
   echo "Es un array de marcas"
   if [[ ! $pet =~ ^\[\] ]]; then
     echo "Hay marcas en el array, se guardarán en el json"
     echo $pet >> script/$var.json
   else
     echo "El Array está vacio, se traerán las marcas del backup"
     cp script/backup/$var.json script/$var.json
   fi
  else
   echo "No, no es un array, se traerán las marcas del backup"
   cp script/backup/$var.json script/$var.json
  fi

done

#proceso para comparador
if [ -f script/comparador.json ];
  then
    echo "Sí, sí existe comparador, movere archivos"
    mv script/comparador.json script/backup/
  else
    echo "No, no existe comparador, no movere archivos"
  fi

  pet="$(curl https://dev.core-comparaya.com/v1/comparador/marcas)"
  if [[ $pet =~ ^\[ ]]; then
   echo "Es un array de marcas"
   if [[ ! $pet =~ ^\[\] ]]; then
     echo "Hay marcas en el array, se guardarán en el json"
     echo $pet >> script/comparador.json
   else
     echo "El Array está vacio, se traerán las marcas del backup"
     cp script/backup/comparador.json script/comparador.json
   fi
  else
   echo "No, no es un array, se traerán las marcas del backup"
   cp script/backup/comparador.json script/comparador.json
  fi
