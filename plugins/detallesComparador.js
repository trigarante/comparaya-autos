import autosService from './ws-autos'

const detallesService ={}

detallesService.search=function ( marca, modelo, submarca,descripcion,accessToken) {
  return autosService.get('v1/comparador/detalles',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{marca,modelo,submarca,descripcion}
  })
}
export default detallesService
