import autosService from './ws-autos'

const modelosService ={}

modelosService.search=function (marca,accessToken) {
  return autosService.get('v1/comparador/modelos',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{marca}
  })
}
export default modelosService
