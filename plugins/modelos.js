import autosService from './ws-autos'

const modelosService ={}

modelosService.search=function (aseguradora, marca,accessToken) {
  return autosService.get('v1/modelos',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{aseguradora,marca}
  })
}
export default modelosService
