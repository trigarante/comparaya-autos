import axios from 'axios'


const wsContactoInteresado = {}

wsContactoInteresado.nuevoContacto = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/interesadoVL',
    data: JSON.parse(peticion)
  })
}
export default wsContactoInteresado
