const endpointsComparaYA = {
  'ABA':
  {
    'marcas': 1092,
    'modelos': 1094,
    'submarcas': 1093,
    'descripciones': 1095,
  },
  'AFIRME':
  {
    'marcas': 1096,
    'modelos': 1100,
    'submarcas': 1099,
    'descripciones': 1101,
  },
  'AIG':
  {
    'marcas': 1102,
    'modelos': 1106,
    'submarcas': 1105,
    'descripciones': 1107,
  },
  'ANA':
  {
    'marcas': 1108,
    'modelos': 1110,
    'submarcas': 1109,
    'descripciones': 1111,
  },
  'ATLAS':
  {
    'marcas': 1119,
    'modelos': 1121,
    'submarcas': 1120,
    'descripciones': 1122,
  },
  'AXA':
  {
    'marcas': 1123,
    'modelos': 1127,
    'submarcas': 1126,
    'descripciones': 1128
  },
  'ELAGUILA':
  {
    'marcas': 1141,
    'modelos': 1145,
    'submarcas': 1144,
    'descripciones': 1146,
  },
  'ELPOTOSI':
  {
    'marcas': 1147,
    'modelos': 1149,
    'submarcas': 1148,
    'descripciones': 1150,
  },
  'GS':
  {
    'marcas': 1151,
    'modelos': 1155,
    'submarcas': 1154,
    'descripciones': 1156,
  },
  'GNP':
  {
    'marcas': 1157,
    'modelos': 1159,
    'submarcas': 1158,
    'descripciones': 1160,
  },
  'HDI':
  {
    'marcas': 1161,
    'modelos': 1165,
    'submarcas': 1164,
    'descripciones': 1166,
  },
  'LATINO':
  {
    'marcas': 203,
    'modelos': 205,
    'submarcas': 204,
    'descripciones': 207
  },
  'MAPFRE':
  {
    'marcas': 1167,
    'modelos': 1171,
    'submarcas': 1170,
    'descripciones': 11723
  },
  'QUALITAS':
  {
    'marcas': 460,
    'modelos': 457,
    'submarcas': 456,
    'descripciones': 458
  },
  'BANORTE':
  {
    'marcas': 1130,
    'modelos': 1133,
    'submarcas': 1132,
    'descripciones': 1134
  },
  'SURA':
  {
    'marcas': 1179,
    'modelos': 1183,
    'submarcas': 1182,
    'descripciones': 1184,
  },
  'BXMAS':
  {
    'marcas': 1185,
    'modelos': 1189,
    'submarcas': 1188,
    'descripciones': 1190
  },
  'ZURICH':
  {
    'marcas': 1191,
    'modelos': 1195,
    'submarcas': 1194,
    'descripciones': 1196,
  }
}

export default endpointsComparaYA;