import axios from "axios";

const cotizacionesService = {};

cotizacionesService.search = function (
  peticion,
  aseguradora,
  accessToken,
  cotizaciones
) {
  let urlInicio = process.env.urlNewCoreCompara + "v1/";
  aseguradora = aseguradora.toLowerCase();
  
 if( aseguradora=='elaguila'){
    aseguradora='aguila'
 }
  return axios({
    method: "post",
    url: process.env.promoCore + `/v2/${aseguradora}/quotation`,
    // url:`https://core-persistance-service.com/v2/${aseguradora}/quotation`,
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${accessToken}`,
    },
    data: JSON.parse(peticion),
  });
};
export default cotizacionesService;
