import axios from "axios";
import dbService from "@/plugins/configBase";

const services = {};

// LOGDATOS
services.saveDataCliente = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: process.env.promoCore + "/v2/json_quotation/customer_record",
    data: JSON.parse(peticion),
  });
};

//BRANDING
services.cotizacionBranding = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: "Bearer " + accessToken },
    url: process.env.promoCore + "/v3/cotizaciones/branding",
    data: JSON.parse(peticion),
  });
};

//ALI
services.cotizacionAli = function (
  peticion,
  accessToken,
  cotizacionAli
) {
  return axios({
    method: "put",
    headers: { Authorization: "Bearer " + accessToken },
    url: process.env.promoCore + `/v1/cotizaciones-ali/${cotizacionAli}`,
    data: JSON.parse(peticion),
  });
};

//COTIZACION
services.cotizacion = function (aseguradora, data, accessToken) {
  aseguradora = aseguradora.toLowerCase();
  if(aseguradora=='elaguila'){
    aseguradora='aguila';
  }
  return axios({
    method: "post",
    headers: { Authorization: "Bearer " + accessToken },
    // url: 'https://core-persistance-service.com/v2/'+`${aseguradora}`+'/quotation',
    url: process.env.promoCore + `/v2/${aseguradora}/quotation`,
    data: JSON.parse(data),
  });
};
//VALIDACION DEL CORREO
services.validarCorreo = function (email) {
  return axios({
    method: "get",
    url: process.env.urlValidaciones + `/validacion/correo?email=` + email,
  });
};
//VALIDACION DEL TELEFONO
services.validarTelefono = function (telefono) {
  return axios({
    method: "get",
    url:
      process.env.urlValidaciones +
      "/validacionTel/validacion?telefono=" +
      telefono,
  });
};
//VALIDACION CODIGO POSTAL
services.validarCodigoPostal = function (cp, accessToken) {
  return axios({
    method: "get",
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${accessToken}`,
    },
    url: process.env.promoCore + "/v2/sepomex/" + cp,
  });
};

//INTERACCION TELEFONICA
services.newLeadInteraccion = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: process.env.promoCore + "/v3/interaction/request_online",
    data: JSON.parse(peticion),
  });
};

//ECOMMERCE
services.newLeadEmision = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: process.env.promoCore + "/v3/issue/request_online",
    data: JSON.parse(peticion),
  });
};

//TELEFONO POR SERVICIO
  services.telService = function (idDiffusionMedium) {
    return axios({
      method: "get",
      url:
        process.env.promoCore +
        "/v1/page/diffusion-medium/phone?idDiffusionMedium=" +
        idDiffusionMedium,
    });
  };

//SERVICIO DE DESCUENTO POR ASEGURADORA
  services.descuentoService = function (aseguradora) {
    return axios({
      method: "get",
      url:
        process.env.promoCore +
        "/v1/discount/" +
        aseguradora +
        "?business=AS&service=PARTICULAR",
    });
  };
//SAVEDATA CAMIONES
  services.saveDataCamiones = function (
    nombreUsr,
    telefonoUsr,
    emailUsr,
    generoUsr,
    codigoPostalUsr,
    edadUsr,
    datosCot,
    respuestaCot,
    emailValid,
    telefonoValid,
    mensajeSMS,
    codigoPostalValid,
    idSubRamo,
    idMedioDifusion,
    aseguradora,
    idPagina,
    ip,
    telefonoAS,
    accessToken
  ) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
  
    url: process.env.promoCore + "/v1/truck-tract",
    data: {
      nombreUsr,
      telefonoUsr,
      emailUsr,
      generoUsr,
      codigoPostalUsr,
      edadUsr,
      datosCot,
      respuestaCot,
      emailValid,
      telefonoValid,
      mensajeSMS,
      codigoPostalValid,
      idSubRamo,
      idMedioDifusion,
      aseguradora,
      idPagina,
      ip,
      telefonoAS,
    },
  });
  };
//GENERAL
  services.general = function (
    nombreUsr,
    telefonoUsr,
    emailUsr,
    generoUsr,
    codigoPostalUsr,
    edadUsr,
    datosCot,
    respuestaCot,
    emailValid,
    telefonoValid,
    mensajeSMS,
    codigoPostalValid,
    idSubRamo,
    idMedioDifusion,
    aseguradora,
    idPagina,
    ip,
    telefonoAS,
    accessToken
  ) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    //GENERAL SALESFORCE
    url: process.env.promoCore + "/v3/general/car",
    data: {
      nombreUsr,
      telefonoUsr,
      emailUsr,
      generoUsr,
      codigoPostalUsr,
      edadUsr,
      datosCot,
      respuestaCot,
      emailValid,
      telefonoValid,
      mensajeSMS,
      codigoPostalValid,
      idSubRamo,
      idMedioDifusion,
      aseguradora,
      idPagina,
      ip,
      telefonoAS,
    },
  });
  };




export default services;
