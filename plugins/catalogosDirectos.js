import autosService from "./ws-autos";

let catalogos = {
  aba: process.env.catalogoABA + "/v2/aba-car",
  afirme: process.env.catalogoAFIR + "/v4/afirme-CAR",
  aig: process.env.catalogoAIG + "/v4/aig-CAR",
  ana: process.env.catalogoANA + "/v1/ana-car",
  atlas: process.env.catalogoAtlas + "/v2/atlas-car",
  axa: process.env.catalogosAXA + "/v1/axa-car",
  banorte: process.env.catalogoBAN + "/v4/banorte-CAR",
  latino: process.env.catalogoLATINO + "/v4/latino-CAR", 
  gnp: process.env.catalogoGNP + "/v3/gnp-car",
  qualitas: process.env.catalogoQ + "/v3",
  generaldeseguros: process.env.catalogoGS + "/v1/gs-car",
  hdi: process.env.catalogoHDI + "/v4/hdi-car",
  mapfre: process.env.catalogoMAP + "/v4/mapfre-CAR",
  sura: process.env.catalogoSURA + "/v1/sura-car",
  elaguila: process.env.catalogosAGUILA + `/v2/elaguila-car`,
  migo: process.env.catalogoMIGO + "/v2/migo-car",
  zurich: process.env.catalogoZurich + "/v1/zurich-car",
  elpotosi: process.env.catalogoPotosi + "/v1/el-potosi-car",
  bxmas: process.env.catalogoBxmas + "/v1/bxmas-car",
};

class CatalogosDirectos {
  marcas(aseguradora) {
    aseguradora = aseguradora.toLowerCase();
    if (
      aseguradora == "uber" ||
      aseguradora == "didi" ||
      aseguradora == "cabify" ||
      aseguradora == "taxi" ||
      aseguradora == "beat"
    ) {
      return autosService.get(catalogos.qualitas + `/qualitas-private/brands`);
    } else if (aseguradora == "qualitas" || aseguradora == "inbursa") {
      return autosService.get(catalogos.qualitas + `/qualitas-car/brands`);
    } else if (aseguradora == "camiones") {
      return autosService.get(catalogos.camiones + `/marcas`);
    } else {
      if (aseguradora == "miituo") {
        aseguradora = "hdi";
      }
      return autosService.get(catalogos[aseguradora] + `/brands`);
    }
  }

  modelos(aseguradora, marca) {
    aseguradora = aseguradora.toLowerCase();
    if (
      aseguradora == "uber" ||
      aseguradora == "didi" ||
      aseguradora == "cabify" ||
      aseguradora == "taxi" ||
      aseguradora == "beat"
    ) {
      return autosService.get(
        catalogos.qualitas + `/qualitas-private/years?brand=${marca}`
      );
    } else if (aseguradora == "qualitas" || aseguradora == "inbursa") {
      return autosService.get(
        catalogos.qualitas + `/qualitas-car/years?brand=${marca}`
      );
    } else if (aseguradora == "camiones") {
      return autosService.get(catalogos.camiones + `/modelos/` + marca, {
        params: {},
      });
    } else {
      if (aseguradora == "miituo") {
        aseguradora = "hdi";
      }
      return autosService.get(catalogos[aseguradora] + `/years?brand=${marca}`);
    }
  }

  submarcas(aseguradora, marca, modelo) {
    aseguradora = aseguradora.toLowerCase();
    if (
      aseguradora == "uber" ||
      aseguradora == "didi" ||
      aseguradora == "cabify" ||
      aseguradora == "taxi" ||
      aseguradora == "beat"
    ) {
      return autosService.get(
        catalogos.qualitas +
          `/qualitas-private/models?brand=${marca}&year=${modelo}`
      );
    } else if (aseguradora == "qualitas" || aseguradora == "inbursa") {
      return autosService.get(
        catalogos.qualitas +
          `/qualitas-car/models?brand=${marca}&year=${modelo}`
      );
    } else if (aseguradora == "camiones") {
      return autosService.get(catalogos.camiones + `/descripcion/` + modelo);
    } else {
      if (aseguradora == "miituo") {
        aseguradora = "hdi";
      }
      return autosService.get(
        catalogos[aseguradora] + `/models?brand=${marca}&year=${modelo}`
      );
    }
  }

  descripciones(aseguradora, marca, modelo, submarca) {
    aseguradora = aseguradora.toLowerCase();
    if (
      aseguradora == "uber" ||
      aseguradora == "didi" ||
      aseguradora == "cabify" ||
      aseguradora == "taxi" ||
      aseguradora == "beat"
    ) {
      return autosService.get(
        catalogos.qualitas +
          `/qualitas-private/variants?brand=${marca}&year=${modelo}&model=${submarca}`
      );
    } else if (aseguradora == "qualitas" || aseguradora == "inbursa") {
      return autosService.get(
        catalogos.qualitas +
          `/qualitas-car/variants?brand=${marca}&year=${modelo}&model=${submarca}`
      );
    } else if (aseguradora == "camiones") {
      return autosService.get(catalogos.camiones + "/tipo/" + submarca);
    } else {
      if (aseguradora == "miituo") {
        aseguradora = "hdi";
      }
      return autosService.get(
        catalogos[aseguradora] +
          `/variants?brand=${marca}&year=${modelo}&model=${submarca}`
      );
    }
  }

}

export default CatalogosDirectos;
