import axios from 'axios'


const wsLeadVendido = {}

wsLeadVendido.nuevoLeadVendido = function (peticion) {

    return axios({
        method: "post",
        url: process.env.urlNewCoreCompara + 'v1/leads',
        data: JSON.parse(peticion)
    })
}
export default wsLeadVendido