import axios from "axios";
import dbService from "./configBase";

const detallesAseguradoras ={}

detallesAseguradoras.search=function (aseguradora, marca, modelo, descripcion,subdescripcion, accessToken) {
    return axios({
        method: "get",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: dbService.baseUrl+  'v1/extras/aseguradoras'+'?marca='+marca+'&subMarca='+descripcion+'&modelo='+modelo+'&descripcion='+subdescripcion+'&transmision=AUT'
    })
}
export default detallesAseguradoras
