import autosService from './ws-autos'

const descripcionesService ={}

descripcionesService.search=function (marca, modelo, accessToken) {
  return autosService.get('v1/comparador/submarcas',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{marca,modelo}
  })
}
export default descripcionesService
