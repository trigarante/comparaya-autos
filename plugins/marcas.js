import autosService from './ws-autos'

const marcasService ={}

marcasService.search=function (aseguradora, accessToken) {
  return autosService.get('v1/marcas',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{aseguradora}
  })
}
export default marcasService



