import axios from 'axios'


const cotizacionService = {}

cotizacionService.search = function (peticion, accessToken) {

  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: process.env.urlNewCoreCompara + 'v1/cotizaciones_autos',
    data: JSON.parse(peticion)
  })
}
export default cotizacionService


