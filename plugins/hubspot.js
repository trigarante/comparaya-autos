import axios from "axios";

const hubspot = {};

hubspot.saveHubspot = function (data) {
    return axios({
      method: "post",
      headers: { "Content-Type": "application/json" },
      url: process.env.hubspot,
      data: JSON.parse(data),
    });

};
export default hubspot;