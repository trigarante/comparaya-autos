module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "ComparaYa",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      // { hid: 'description', name: 'description', content: 'Template to pages from mejorseguro' }
    ],
    link: [{ rel: "icon", type: "image/png", href: "./favicon.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    //analyze:true,
    //vendor: ['jquery', 'bootstrap'],
    /*
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          //loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include bootstrap css
  css: [
    "static/css/bootstrap.min.css",
    "static/css/styles.min.css",
    "static/css/circle.css",
  ],
  // include bootstrap js on startup
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],

  modules: ["@nuxtjs/axios"],
  env: {
    urlNewCoreCompara: "https://dev.core-comparaya.com/", // CORE
    // SERVICIOS CATALOGOS
    // CATALOGO
    catalogoLATINO: "https://service-dev.ws-latino.com",
    catalogoABA: "https://dev.ws-abaseguros.com",
    catalogoAIG: "https://service-dev.ws-aig.com", 
    catalogoANA: "https://dev.ws-ana.com",
    catalogoAFIR: "https://dev.ws-afirme.com",
    catalogoBAN: "https://service-dev.ws-banort.com",
    catalogoGS: "https://dev.ws-general.com",
    catalogoHDI: "https://dev.ws-hdiseguro.com",
    catalogoGNP: "https://dev.web-gnp.mx",
    catalogoQ: "https://dev.ws-qualitas.com",
    catalogoMAP: "https://service-dev.ws-mapfre.com",
    catalogosAXA: "https://dev.ws-axxa.com",
    catalogosAGUILA: "https://dev.ws-aguila.com",
    catalogoSURA: "https://dev.ws-sura.com",
    catalogoMIGO: "https://dev.ws-migo.com",
    catalogoZurich: "https://dev.ws-zurich.com",
    catalogoPotosi: "https://dev.ws-elpotosi.com",
    catalogoAtlas: "https://dev.wsatlas.com",
    catalogoCamiones: "https://ws-camiones.com",
    catalogoBxmas: "https://dev.ws-bxmas.com", //
    //FIN CATALOGOS
    //INICIA MOTORES
    urlSitio: "https://p.comparaya.mx/seguros-de-autos/",
    motorAfirme: "https://p.afirme-comprasegura.com/cliente",
    motorAba: "https://p.aba-comprasegura.com",
    motorQualitas: "https://p.qualitas-comprasegura.com/cliente",
    motorGnp: "https://p.comparaya.mx/seguros-de-autos/gnp/compra/cliente",
    motorHdi: "https://p.hdi-comprasegura.com",
    motorAxa: "https://p.axa-comprasegura.com/cliente",
    motorMigo: "https://p.migo-comprasegura.com",
    motorAtlas: "https://p.atlas-comprasegura.com",
    motorAguila: "https://p.comparaya.mx/seguros-de-autos/el-aguila/compra/cliente",
    motorLatino: "https://p.comparaya.mx/seguros-de-autos/la-latino/compra/cliente",
    motorPotosi: "https://p.elpotosi-comprasegura.com",
    motorAna: "https://p.ana-comprasegura.com",
    motorSura: "https://p.comparaya.mx/seguros-de-autos/sura/compra/cliente",
    motorAig: "https://p.comprasegura.mx/aig/cliente",
    motorGds: "https://p.generaldeseguros-comprasegura.com",
    motorBanorte: "https://p.comparaya.mx/seguros-de-autos/seguros-banorte/compra/cliente",
    motorBxmas: "https://p.comparaya.mx/seguros-de-autos/ve-por-mas/compra/cliente",
    motorMapfre: "https://p.comprasegura.mx/mapfre/cliente",
    motorZurich: "https://p.comparaya.mx/seguros-de-autos/zurich/compra/cliente",
    //FIN MOTORES
    urlValidaciones: "https://core-blacklist-service.com/rest", // VALIDACIONES
    urlNewDataBase: "https://ahorraseguros.mx/servicios",
    urlDB: "https://ahorraseguros.mx/ws-rest/servicios",
    promoCore: "https://dev.core-persistance-service.com",
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    
    Environment: "DEVELOP",
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    gzip: { threshold: 9 },
  },
  router: {
    base: "/seguros-de-autos/",
  },
};
