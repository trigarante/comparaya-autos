import Vuex from "vuex";
import cotizacion from "~/plugins/cotizacion";
import getTokenService from "../plugins/getToken";
import validacionesService from "../plugins/validaciones";
import newProspectNewCoreService from "../plugins/saveProspectoNewCoreNormal";
import cotizacionAliService from "../plugins/cotizacionesAliNewCore";
import wsLeadVendido from "../plugins/wsSendLead";
import dbService from "../plugins/configBase";
import cotizacionBranding from "../plugins/cotizacionesBranding";
import cotizacionesService from "~/plugins/cotizaciones";
import saveDataCliente from "~/plugins/saveDataCliente";
import hubspot from "~/plugins/hubspot";
import telApi from "~/plugins/telefonoService2.js";

const createStore = () => {
  const validacion = new validacionesService();
  return new Vuex.Store({
    state: {
      msjEje: false,
      ambientePruebas: false,
      asegRecNombre: "GNP",
      asegRecomendada: [],
      insurers: [],
      clickAseguradoraRecomendada: false,
      cargandocotizacion: false,
      cargandoCotizaciones: false,
      sin_token: false,
      tiempo_minimo: 1.3,
      limpiaCoberturas: "-",
      clickInteraccion: false,
      clickEcommerce: false,
      isMigo: false,
      cotizacionInterna: false,
      mostrarBoton: false,
      control: {
        vista: true,
        modalAseguradora: "0",
      },
      ventaLeads: "",
      interaccionBtnDisabled: false,
      config: {
        time: 0,
        reload: true,
        motor: false,
        corePer: false,
        respuesta: "",
        precio: "",
        eCotizar: false,
        cotizaciones: false,
        ekomi: false,
        catalogo_directo: false,
        dataAli: false,
        cotizacionAli: "",
        paqueteCotizacion: "AMPLIA",
        nodoModal: 0,
        nodoModal2: 0,
        aseguradora: "",
        aseguradoraCatalogos: "",
        ventaLead: "",
        cotizacion: false,
        emision: false,
        descuento: 0,
        descAfirme: 0,
        telefonoAS: "",
        grupoCallback: "",
        from: "",
        idPagina: 187,
        idMedioDifusion: 2402,
        idCampana: 0,
        idSubRamo: 50,
        habilitarBtnEmision: false,
        habilitarBtnInteraccion: false,
        btnEmisionDisabled: false,
        loading: true,
        urlRedireccion: "",
        numCotizacionUsuario: "",
        idProspecto: 0,
        idCotizacion: 0,
        idCotizacion_a: 0,
        idEjecutivo: 0,
        estado: "",
        dto: "",
        msi: "",
        promoLabel: "",
        promoSpecial: false,
        promoImg: "",
        extraMsg: "",

        cobertura: {
          amplia_premium: 1,
          amplia_clasica: 2,
          amplia_ahorra: 3,
          amplia_esencial: 4,
          amplia_limitada: 5,
          rc: 6,
        },
        accessToken: "",
        logoAseguradora: {
          hdi: "https://sfo3.digitaloceanspaces.com/prod-mx/prod_asmx/ahorraseguros.mx/wp-content/themes/nexoslab/assets/logotipos/hdi/logo.svg",
        },
        agenteAutorizado:
          "https://sfo3.digitaloceanspaces.com/prod-mx/prod_asmx/ahorraseguros.mx/wp-content/themes/nexoslab/assets/logotipos/agente.svg",
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        idHubspot: '',
        urlOrigen: "",
        aseguradora: "",
        marca: "",
        modelo: "",
        descripcion: "",
        detalle: "",
        clave: "",
        cp: "",
        nombre: "",
        telefono: "",
        gclid_field: "",
        correo: "",
        edad: "",
        fechaNacimiento: "",
        genero: "",
        emailValid: "",
        telefonoValid: "",
        codigoPostalValid: "",
        dominioCorreo: "",
        idLogData: "",
        matchtype: "",
        fbclid: "",
      },
      aseguradoras: {
        ABA: {
          nombre: "ABA",
          nombre2: "ABA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 39,
          id_pagina: 218,
          idMedioDifusion: 1506,
          idInteraccion: 1506,
          idEecomerce: 2285,
          id_campana: 126,
          callback: "VN ABA",
          DID: " 5588902269",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "5.0",
          phonecotizar: "(55)88902269",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy popular",
          promo: "¡Increíbles descuentos!. + 3, 6 y 12 MSI.",
          robo_total: "Valor Comercial,     Deducible 10%",
          daños_vehiculo: "Valor Comercial,   Deducible 5%",
          responsabilidad_civil: "$ 3,000,000",
          proteccion: 4.5,
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "¡Increíbles descuentos! + 3, 6 12 MSI.",
          //alttext: 'ABA',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        ABA2: {
          nombre: "ABA2",
          nombre2: "ABA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 39,
          id_pagina: 218,
          idMedioDifusion: 1506,
          idInteraccion: 1506,
          idEecomerce: 2285,
          id_campana: 126,
          callback: "VN ABA",
          DID: " 5588902269",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "5.0",
          phonecotizar: "(55)88902269",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy popular",
          promo: "¡Increíbles precios! ó 3 y 6 MSI.",
          robo_total: "Valor Comercial,    Deducible 10%",
          daños_vehiculo: "Valor Comercial,   Deducible 5%",
          responsabilidad_civil: "$ 3,000,000",
          proteccion: 4.5,
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "¡Increíbles precios! ó 3 y 6 MSI.",
          //alttext: 'ABA',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        ANA: {
          nombre: "ANA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 46,
          id_pagina: 234,
          idMedioDifusion: 1522,
          idInteraccion: 1522,
          idEecomerce: 2302,
          id_campana: 126,
          callback: "VN ANA",
          DID: "5588902340",
          cotizacion: true,
          emision: true,
          descuento: 20,
          calificacion: "4.9",
          phonecotizar: "(55)88902340",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy Popular",
          promo: "Hasta 40% de desc + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: " Hasta 40% de desc + 3, 6 y 12 MSI.",
          //alttext: 'ANA',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        ATLAS: {
          nombre: "ATLAS",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 47,
          id_pagina: 230,
          idMedioDifusion: 1518,
          idInteraccion: 1518,
          idEecomerce: 2297,
          id_campana: 126,
          callback: "VN ATLAS",
          DID: "5547495143",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.8",
          phonecotizar: "(55)47495143",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy Popular",
          promo: "50% de descuento + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "50% de descuento + 3, 6 y 12 MSI.",
          //alttext: 'ATLAS',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        AXA: {
          nombre: "AXA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 45,
          id_pagina: 221,
          idMedioDifusion: 1509,
          idInteraccion: 1509,
          idEecomerce: 2288,
          id_campana: 126,
          callback: "VN AXA",
          DID: " 5588902232",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.7",
          phonecotizar: "(55)88902232",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy popular",
          promo: "15% de desc + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "15% de desc + 3, 6 y 12 MSI.",
          //alttext: 'AXA',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        BANORTE: {
          nombre: "BANORTE",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 38,
          id_pagina: 222,
          idMedioDifusion: 1510,
          idInteraccion: 1510,
          idEecomerce: 2289,
          id_campana: 126,
          callback: "VN BANORTE",
          DID: "5588902228",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.6",
          phonecotizar: "(55)88902228",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "Obtén 30% de desc + 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "Obtén 30% de desc + 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        GNP: {
          nombre: "GNP",
          nombre2: "GNP",
          asg_rec: true, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 40,
          id_pagina: 220,
          idMedioDifusion: 1508,
          idInteraccion: 1508,
          idEecomerce: 2287,
          id_campana: 126,
          callback: "VN GNP",
          DID: "5588902239",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.5",
          phonecotizar: "(55)88902239",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy popular",
          promo: "5% de descuento + 3, 6 y 12 MSI.",
          robo_total: "Valor Convenido, Deducible 10%",
          daños_vehiculo: "Valor Convenido, Deducible 5%",
          responsabilidad_civil: "$ 3,000,000",
          proteccion: 4.5,
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "5% de descuento + 3, 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        HDI: {
          nombre: "HDI",
          nombre2: "HDI",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 32,
          id_pagina: 224,
          idMedioDifusion: 1512,
          idInteraccion: 1512,
          idEecomerce: 2291,
          id_campana: 126,
          callback: "VN HDI",
          DID: " 5588902230",
          cotizacion: true,
          emision: true,
          descuento: 10,
          calificacion: "5.0",
          phonecotizar: "(55)88902230",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy popular",
          promo: "Hasta 30% de desc. + 6 y 12 MSI.",
          robo_total: "Valor Comercial, Deducible 10%",
          daños_vehiculo: "Valor Comercial, Deducible 5%",
          responsabilidad_civil: "$ 3,000,000",
          proteccion: 4.5,
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "40% de desc. + 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        HDI2: {
          nombre: "HDI2",
          nombre2: "HDI",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          //logo_aseg: 'HDI',
          idSubRamo: 32,
          id_pagina: 224,
          idMedioDifusion: 1512,
          idInteraccion: 1512,
          idEecomerce: 2291,
          id_campana: 70,
          callback: "VN HDI",
          DID: "5588902230",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "5.0",
          phonecotizar: "(55)88902230",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy popular",
          promo: "25% de descuento + 6 y 12 MSI.",
          robo_total: "Valor Comercial, Deducible 10%",
          daños_vehiculo: "Valor Comercial, Deducible 5%",
          responsabilidad_civil: "$ 3,000,000",
          proteccion: 4.5,
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "25% de descuento + 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        QUALITAS: {
          nombre: "QUALITAS",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 35,
          id_pagina: 219,
          idMedioDifusion: 1507,
          idInteraccion: 1507,
          idEecomerce: 2286,
          id_campana: 126,
          callback: "VN QUALITAS",
          DID: " 5588902221",
          cotizacion: true,
          emision: true,
          descuento: 20,
          calificacion: "5.0",
          phonecotizar: "(55)88902221",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★★",
          popularidad: "Muy popular",
          promo: "Recibe 30% de desc + 3,6.9 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "Recibe 30% de desc + 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        MAPFRE: {
          nombre: "MAPFRE",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 43,
          id_pagina: 235,
          idMedioDifusion: 1523,
          idInteraccion: 1523,
          idEecomerce: 2303,
          id_campana: 126,
          callback: "VN Mapfre",
          DID: "5588902266",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.2",
          phonecotizar: "(55)88902266",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "30% de desc. + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "30% de desc. + 3, 6 y 12 MSI.",
          estatus_cotizacion: true,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        ELAGUILA: {
          nombre: "ELAGUILA",
          nombre2: "EL AGUILA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 55,
          id_pagina: 223,
          idMedioDifusion: 1511,
          idInteraccion: 1511,
          idEecomerce: 2290,
          id_campana: 126,
          callback: "VN EL AGUILA",
          DID: " 5547495125",
          cotizacion: false,
          emision: true,
          descuento: 5,
          calificacion: "4.1",
          phonecotizar: "(55)47495125",
          horario_atencion: "L-V 9 a.m. a 7 p.m. y S 9 a.m. a 1 p.m.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "35% de desc. + 3, 6 y 12 MSI.",
          robo_total: "Valor Comercial, Deducible 10%",
          daños_vehiculo: "Valor Comercial, Deducible 5%",
          responsabilidad_civil: "$ 1,000,000",
          proteccion: 4.5,
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "35% de desc. + 3, 6 y 12 MSI.",
          estatus_cotizacion: true,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],

          orden: 0,
        },
        ELAGUILA2: {
          nombre: "ELAGUILA2",
          nombre2: "EL AGUILA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 55,
          id_pagina: 223,
          idMedioDifusion: 1511,
          idInteraccion: 1511,
          idEecomerce: 2290,
          id_campana: 126,
          callback: "VN EL AGUILA",
          DID: " 5547495125",
          cotizacion: false,
          emision: true,
          descuento: 5,
          calificacion: "4.1",
          phonecotizar: "(55)47495125",
          horario_atencion: "L-V 9 a.m. a 7 p.m. y S 9 a.m. a 1 p.m.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "¡Increíbles precios! + 3, 6 y 12 MSI.",
          robo_total: "Valor Comercial, Deducible 3%",
          daños_vehiculo: "Valor Comercial, Deducible 3%",
          responsabilidad_civil: "$ 1,000,000",
          proteccion: "5.0",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "¡Increíbles precios! + 3, 6 y 12 MSI.",
          estatus_cotizacion: true,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          orden: 0,
        },
        SURA: {
          nombre: "SURA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 34,
          id_pagina: 232,
          idMedioDifusion: 1520,
          idInteraccion: 1520,
          idEecomerce: 2300,
          id_campana: 126,
          callback: "VN SURA",
          DID: " 5588801532",
          cotizacion: true,
          emision: false,
          descuento: 10,
          calificacion: "4.0",
          phonecotizar: "(55)88801532",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          promo: "Hasta 15% de desc + 3 y 6 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "Hasta 15% de desc + 3 y 6 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        ZURICH: {
          nombre: "ZURICH",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 31,
          id_pagina: 227,
          idMedioDifusion: 1515,
          idInteraccion: 1515,
          idEecomerce: 2294,
          id_campana: 126,
          callback: "VN ZURICH",
          DID: "5588902273",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.0",
          phonecotizar: "(55)88902273",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "20% de desc. + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "20% de desc. + 3, 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        AFIRME: {
          nombre: "AFIRME",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 49,
          id_pagina: 228,
          idMedioDifusion: 1516,
          idInteraccion: 1516,
          idEecomerce: 2295,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: " (55)88902309",
          cotizacion: true,
          emision: true,
          descuento: 10,
          calificacion: "4.0",
          phonecotizar: "(55)88902309",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "Hasta 40% de desc + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "Hasta 40% de desc + 3, 6 y 12 MSI.",
          estatus_cotizacion: false,
          robo_total: "Valor Comercial,     Deducible 10%",
          daños_vehiculo: "Valor Comercial,   Deducible 5%",
          responsabilidad_civil: "$ 2,500,000",
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        AIG: {
          nombre: "AIG",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 44,
          id_pagina: 229,
          idMedioDifusion: 1517,
          idInteraccion: 1517,
          idEecomerce: 2296,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: "5528814666",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.0",
          phonecotizar: "(55)28814666",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "¡Increíbles descuentos! + 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "¡Increíbles descuentos! + 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        ELPOTOSI: {
          nombre: "ELPOTOSI",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 33,
          id_pagina: 231,
          idMedioDifusion: 1519,
          idInteraccion: 1519,
          idEecomerce: 2299,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: " 5588801534",
          cotizacion: true,
          emision: true,
          descuento: 30,
          calificacion: "4.0",
          phonecotizar: "(55)88801534",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "20% de desc. + 6 y 12 MSI.",
          robo_total: "Valor Comercial, Deducible 10%",
          daños_vehiculo: "Valor Comercial, Deducible 5%",
          responsabilidad_civil: "$ 3,000,000",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "20% de desc. + 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        MIGO: {
          nombre: "MIGO",
          nombrebtn: "MIGO",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 30,
          id_pagina: 237,
          idMedioDifusion: 1525,
          idInteraccion: 1525,
          idEecomerce: 2305,
          id_campana: 151,
          callback: "VN Agregadores",
          DID: "5528815933",
          cotizacion: true, //se apago 4/10/21
          emision: true,
          descuento: 35,
          calificacion: "4.0",
          phonecotizar: "(55)28815933",
          robo_total: "Valor Comercial,     Deducible 0%",
          daños_vehiculo: "Valor Comercial,   Deducible 10%",
          responsabilidad_civil: "$ 1,500,000",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "Hasta 25% de desc + 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "Hasta 25% de desc + 6 y 12 MSI.",
          text_poppers: "seguros MIGO",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          habilitar: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        GS: {
          nombre: "GS",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 42,
          id_pagina: 236,
          idMedioDifusion: 1524,
          idInteraccion: 1524,
          idEecomerce: 2304,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: "5588801536",
          cotizacion: true,
          emision: true,
          descuento: 25,
          calificacion: "4.0",
          phonecotizar: "(55)88801536",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "20% de descuento + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "20% de descuento + 3, 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        /*ES GENERAL DE SEGURO PERO LO CAMBIARON A GS*/
        GS: {
          nombre: "GS",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 42,
          id_pagina: 236,
          idMedioDifusion: 1524,
          idInteraccion: 1524,
          idEecomerce: 2304,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: "5588801536",
          cotizacion: true,
          emision: true,
          descuento: 25,
          calificacion: "4.0",
          phonecotizar: "(55)88801536",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "20% de descuento + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "20% de descuento + 3, 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        INBURSA: {
          nombre: "INBURSA",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 37,
          id_pagina: 225,
          idMedioDifusion: 1513,
          idInteraccion: 1513,
          idEecomerce: 2292,
          id_campana: 126,
          callback: "VN INBURSA",
          DID: "5547495128",
          cotizacion: false,
          emision: true,
          descuento: 0,
          calificacion: "4.0",
          phonecotizar: "(55)47495128",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "Hasta 10% desc + 3,6,9 y 12 MSI.",
          textogenerico: "6 MSI con tarjetas Inbursa",
          textbadget: "Hasta 10% desc + 3,6,9 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        LALATINO: {
          nombre: "LALATINO",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 41,
          id_pagina: 226,
          idMedioDifusion: 1514,
          idInteraccion: 1514,
          idEecomerce: 2293,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: "5588902330",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.0",
          phonecotizar: "(55)47495118",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: " Hasta 50% de desc. + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: " Hasta 50% de desc. + 3, 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        LATINO: {
          nombre: "LATINO",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 41,
          id_pagina: 226,
          idMedioDifusion: 1514,
          idInteraccion: 1514,
          idEecomerce: 2293,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: "5588902330",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.0",
          phonecotizar: "(55)47495118",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: " 50% de desc + 3, 6 y 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: " 50% de desc + 3, 6 y 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        CRABI: {
          nombre: "CRABI",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 33,
          id_pagina: 159,
          idMedioDifusion: 1514,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: "5547495118",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: "4.0",
          phonecotizar: "(55)47495118",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "¡Increíbles precios! + 12 MSI.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "¡Increíbles precios! + 12 MSI.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        PRIMEROSEGUROS: {
          nombre: "PRIMEROSEGUROS",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 17,
          id_pagina: 1161,
          id_campana: 43,
          callback: "VN Multimarcas",
          DID: "5547495142",
          cotizacion: false,
          emision: false,
          descuento: 0,
          calificacion: "4.0",
          phonecotizar: "(55)47495142",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "Consigue mejor precio por teléfono",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "Consigue mejor precio por teléfono",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
        BXMAS: {
          nombre: "BXMAS",
          asg_rec: false, //aseguradora recomendada - BANDERA
          asg_rec_config: [], // configuracion aseguradora recomendada
          idSubRamo: 36,
          id_pagina: 233,
          idMedioDifusion: 1521,
          idInteraccion: 1521,
          idEecomerce: 2301,
          id_campana: 126,
          callback: "VN Multimarcas",
          DID: " 5588807357",
          cotizacion: true,
          emision: false,
          descuento: 20,
          calificacion: "4.0",
          phonecotizar: "(55)88807357",
          horario_atencion: "L-V 8 a 21 hrs. / S 9 a 14 hrs.",
          stars: "★★★★☆",
          popularidad: "Popular",
          promo: "Obtén 20% de descuento.",
          textogenerico: "Este es el mejor precio para tu seguro",
          textbadget: "Obtén 20% de descuento.",
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: "Diferenciador 1",
            },
            {
              d2: "Diferenciador 2",
            },
            {
              d3: "Diferenciador 3",
            },
          ],
          orden: 0,
        },
      },
      solicitud: {},
      cotizacion: {},
      cotizaciones: [],
      dataCotizacion: {},
      dataCotizacionMigo: {},
      dataCotizacionGnp: {},
      emailValid: 1,
      telefonoValid: 1,
      codigoPostalValid: 1,
    },
    actions: {
      getToken() {
        return new Promise((resolve, reject) => {
          getTokenService.search(dbService.tokenData).then(
            (resp) => {
              if (typeof resp.data == "undefined") {
                this.state.config.accessToken = resp.accessToken;
              } else if (typeof resp.data.accessToken != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },

      sleep: function () {
				return new Promise((resolve) => setTimeout(resolve, this.state.config.time));
			},
    },
    mutations: {

			saveHubspot: async function (state) {

        const socios = {
          "AFIRME": "SEGUROS AFIRME",
          "ANA": "ANA SEGUROS",
          "ATLAS": "SEGUROS ATLAS",
          "ELAGUILA": "EL AGUILA",
          "GENERALDESEGUROS": "GENERAL DE SEGUROS",
          "LATINO": "LA LATINO",
          "ELPOTOSI": "EL POTOSI",
          "PRIMEROSEGUROS": "PRIMERO SEGUROS",
          "BXMAS": "BX+",

          default: state.config.aseguradora,
        }

				const data = {
					mobilephone: state.formData.telefono,
					email: state.formData.correo,
					firstname: state.formData.nombre,
					lastname: "",
					marca: state.formData.marca,
					modelo: state.formData.modelo,
					submarca: state.formData.descripcion,
					socio: socios[state.config.aseguradora] ?? socios.default,
				};
				hubspot.saveHubspot(JSON.stringify(data))
					.then((resp) => {
						state.formData.idHubspot = resp.data.id;
						this.commit("saveDataCliente");
					})
					.catch((error) => {
						console.log(error)
						state.formData.idHubspot =
              typeof error.response == "undefined"
                ? "{}"
                : error.response.data.message;
            this.commit("saveDataCliente");
					});
			},
      telApi: function (state) {
        try {
          telApi
            .search(state.config.idMedioDifusion)
            .then((resp) => {
              resp = resp.data;
              let telefono = resp.telefono;
              if (parseInt(telefono)) {
                var tel = telefono + "";
                var tel2 = tel.substring(2, tel.length);
                state.config.telefonoAS = tel2;
              }
            })
            .catch((error) => {
              console.log(error);
            });
        } catch (error) {
          console.log(error);
        }
      },
      validarTokenCore: function (state) {
        try {
          if (process.browser) {
            if (localStorage.getItem("authToken") === null) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
      saveDataCliente: function (state) {
       state.formData.dominioCorreo = state.formData.dominioCorreo;
        let peticionProspecto = {
          aseguradora: state.config.aseguradora,
          clave: state.formData.clave,
          cp: state.formData.cp,
          descripcion: state.formData.descripcion,
          detalle: state.formData.detalle,
          descuento: state.config.descuento,
          edad: state.formData.edad,
          fechaNacimiento: (
            "01/01/" +
            (new Date().getFullYear() - state.formData.edad).toString()
          ).toString("yyyyMM/dd"),
          genero: state.formData.genero, //=== "M" ? "MASCULINO" : "FEMENINO",
          marca: state.formData.marca,
          modelo: state.formData.modelo,
          movimiento: "cotizacion",
          paquete: "AMPLIA",
          servicio: "PARTICULAR",          
        };
        state.config.dataPeticionCotizacion = peticionProspecto;
        let datosCliente;
        datosCliente = {
          datosCot: JSON.stringify(state.formData),
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          idSubRamo: state.config.idSubRamo,
          peticionesAli: JSON.stringify(peticionProspecto),
        };

        this.commit("enhancedConversionGTAG");
        saveDataCliente
          .search(JSON.stringify(datosCliente), state.config.accessToken)
          .then((resp) => {
            //console.log("Registro Datos Cliente - OK ");
            // console.log(resp);
            state.formData.idLogData = resp.data.data.idLogClient;
            this.commit("saveProspecto");
          })
          .catch((error) => {
            var status = "ERR_CONNECTION_REFUSED";
            if (typeof error.status != "undefined") {
              status = error.status;
            } else if (typeof error.response != "undefined") {
              if (typeof error.response.status != "undefined") {
                status = error.response.status;
              }
            }
            if (status === 401) {
              dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("saveDataCliente");
                })
                .catch((error) => {
                  console.log("Hubo un problema al generar el token");
                  state.idEndPoint = 1;
                  this.catchStatusAndMessage(error, {
                    tokenData: process.env.tokenData,
                  });
                });
            } else {
              state.idEndPoint = 1;
              this.catchStatusAndMessage(error, datosCliente);
            }
          });
      },
      enhancedConversionGTAG: function (state) {
        dataLayer.push({
          event: "Lead",
          phone: "52" + state.formData.telefono,
          email: state.formData.correo,
        });
      },
      validateData: async function (state) {
        let dataCotizacion = {
          aseguradora: state.config.aseguradora,
          clave: state.formData.clave,
          cp: state.formData.cp,
          descripcion: state.formData.descripcion,
          detalle: state.formData.detalle,
          descuento: state.config.descuento,
          edad: state.formData.edad,
          fechaNacimiento: (
            "01/01/" +
            (new Date().getFullYear() - state.formData.edad).toString()
          ).toString("dd/MM/yyyy"),
          genero: state.formData.genero, //=== "M" ? "MASCULINO" : "FEMENINO",
          marca: state.formData.marca,
          modelo: state.formData.modelo,
          movimiento: "cotizacion",
          paquete: state.config.aseguradora == "GNP" ? "ESPECIAL" : "AMPLIA",
          servicio: "PARTICULAR",
        };
        state.config.dataPeticionCotizacion = dataCotizacion;
        validacion
          .validarCorreo(state.formData.correo)
          .then((resp) => {
            if (typeof resp.data == "undefined") {
              state.formData.emailValid = resp.valido;
            } else if (typeof resp.data.valido != "undefined") {
              state.formData.emailValid = resp.data.valido;
            }
            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                if (typeof resp.data == "undefined") {
                  state.formData.telefonoValid = resp.mensaje;
                } else if (typeof resp.data.mensaje != "undefined") {
                  state.formData.telefonoValid = resp.data.mensaje;
                }
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data),
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: state.formData.telefono,
                      });
                this.commit("validacionCP");
              });
          })
          .catch((error) => {
            state.formData.emailValid = true;
            state.idEndPoint = 157;
            state.status =
              error.response !== undefined ? error.response.status : 500;
            state.message =
              error.response !== undefined
                ? JSON.stringify({
                    error: error.response.data,
                    peticion: JSON.parse(error.config.data),
                  })
                : JSON.stringify({
                    error: error,
                    peticion: state.formData.correo,
                  });
            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                if (typeof resp.data == "undefined") {
                  state.formData.telefonoValid = resp.mensaje;
                } else if (typeof resp.data.mensaje != "undefined") {
                  state.formData.telefonoValid = resp.data.mensaje;
                }
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data),
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: state.formData.telefono,
                      });
                this.commit("validacionCP");
              });
          });
      },
      descuentoEstado() {
        validacion
          .descuentoEstado(
            state.config.aseguradora,
            state.config.estado,
            state.config.accessToken
          )
          .then((resp) => {
            if (typeof resp.data == "undefined") {
              state.config.dataPeticionCotizacion.descuento = resp.descuento;
              this.commit("cotizar");
            } else if (typeof resp.data.descuento != "undefined") {
              state.config.dataPeticionCotizacion.descuento =
                resp.data.descuento;
              this.commit("cotizar");
            }
          })
          .catch((error) => {
            state.idEndPoint = 1719;
            this.catchStatusAndMessage(error, state.config.estado);
            this.commit("cotizar");
          });
      },
      validacionCP: function (state) {
        validacion
          .validarCodigoPostal(state.formData.cp, state.config.accessToken)
          .then((resp) => {
            state.formData.codigoPostalValid = resp.data.data.length > 0
            /*this.dispatch("sleep").then(() => {
              this.commit("saveHubspot");
            })*/
             this.commit("saveDataCliente");
          })
          .catch((error) => {
            state.status = error.status
              ? error.status
              : error.response
              ? error.response.status
              : 500;
            state.message = error.data
              ? JSON.stringify({
                  error: error.data,
                  peticion: { cp: state.formData.cp },
                })
              : JSON.stringify({
                  error: error,
                  peticion: { cp: state.formData.cp },
                });
            state.formData.codigoPostalValid = true;
            if (state.status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  // this.commit("validacionCP");
                })
                .catch((error) => {
                  state.idEndPoint = 383;
                  state.status = error.status
                    ? error.status
                    : error.response
                    ? error.response.status
                    : 500;
                  state.message = error.data
                    ? JSON.stringify({
                        error: error.data,
                        peticion: { tokenData: dbService.tokenData },
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                  if (state.config.cotizacion) {
                    if (state.cotizacionInterna)
                      if (state.config.asegDescuento)
                        this.commit("obtenerDescuento");
                      else this.commit("saveProspecto");
                    else this.commit("cotizar");
                  } else {
                    if (state.config.ventaLead == true)
                      this.commit("v1cotizaciones");
                    else this.commit("saveProspecto");
                  }
                });
            } else {
              state.idEndPoint = 223;
              if (state.config.cotizacion) {
                if (state.cotizacionInterna)
                  if (state.config.asegDescuento)
                    this.commit("obtenerDescuento");
                  else this.commit("saveProspecto");
                else this.commit("cotizar");
              } else {
                if (state.config.ventaLead == true)
                  this.commit("v1cotizaciones");
                // else this.commit("saveProspecto");
              }
            }
          });
      },
      cotizarInterno: function (state) {
        if (state.config.cotizaciones) {
          var data = state.config.dataPeticionCotizacion;
        } else {
          var data = {
            Aseguradora: state.config.aseguradora,
            Clave: state.formData.clave,
            Cp: state.formData.cp,
            Descripcion: state.formData.descripcion,
            Detalle: state.formData.detalle,
            Descuento: state.config.descuento,
            Edad: state.formData.edad,
            fechaNacimiento: state.formData.fechaNacimiento,
            Genero: state.formData.genero, //=== "M" ? "MASCULINO" : "FEMENINO",
            Marca: state.formData.marca,
            Modelo: state.formData.modelo,
            Movimiento: "cotizacion",
            Paquete: state.config.aseguradora == "GNP" ? "ESPECIAL" : "AMPLIA",
            Servicio: "PARTICULAR",
          };
        }
        cotizacionesService
          .search(
            JSON.stringify(data),
            state.config.aseguradora,
            localStorage.getItem("authToken"),
            state.config.cotizaciones
          )
          .then((resp) => {
            if (typeof resp.data === "undefined") {
              resp = resp;
            } else {
              resp = JSON.parse(resp.data.data);
            }
            state.cotizacion = resp;
            //console.log(state.cotizacion)
            if (state.cotizacion.cliente == undefined) {
              state.cotizacion.Cliente.Telefono = state.formData.telefono;
            } else {
              state.cotizacion.cliente.telefono = state.formData.telefono;
              state.cotizacion.cliente.email = state.formData.correo;
              state.cotizacion.cliente.fechaNacimiento = state.formData.fechaNacimiento;
              state.cotizacion.cliente.edad = state.formData.edad
            }
            state.mostrarBoton = true;
            state.limpiaCoberturas =
              state.config.aseguradora === "LATINO" ? "" : "-";
            // VALIDACION PARA AFIRME

            if (
              state.config.aseguradora == "ABA" ||
              state.config.aseguradora === "BANORTE" ||
              state.config.aseguradora === "LATINO" ||
              state.config.aseguradora === "QUALITAS" ||
              state.config.aseguradora === "AFIRME" ||
              state.config.aseguradora == "AIG"
            ) {
              state.cotizacion = resp;

              if (typeof resp.cotizacion[0] !== "undefined") {
                resp.cotizacion = resp.cotizacion.filter(
                  (x) => x.amplia
                )[0].amplia[0].anual;
                if (resp.paquete === "ALL") {
                  resp.coberturas = resp.coberturas.filter(
                    (x) => x.amplia
                  )[0].amplia;
                } else if (typeof resp.coberturas[0] !== "undefined") {
                  resp.coberturas = resp.coberturas[0];
                }
              }
              state.mostrarBoton = true;
              // debugger
              if (typeof resp != "string") {
                //IF-PRECIO
                if (
                  Object.keys(resp.cotizacion).includes("primaTotal") &&
                  resp.cotizacion.primaTotal != null &&
                  resp.cotizacion.primaTotal != ""
                ) {
                  state.cargandocotizacion = true;
                  state.config.precio = state.cotizacion.cotizacion.primaTotal;
                  this.commit("cotizacionesAli");
                } else {
                  this.commit("cotizacionesAli");
                }
              } else {
                this.commit("cotizacionesAli");
              }
            } // FIN VALIDACION PARA QUALITAS
            // FIN VALIDACION PARA HDI
            // VALIDACION MIGO
            else if (
              state.config.aseguradora === "MIGO" ||
              state.config.aseguradora === "GNP"
            ) {
              state.cotizacion = resp;
              if (
                typeof resp.coberturas != "undefined" &&
                typeof resp.coberturas[0] != "undefined" &&
                typeof resp.coberturas[0][0] != "undefined"
              ) {
                resp.coberturas = resp.coberturas[0]; //2020
              } else if (
                typeof resp.coberturas != "undefined" &&
                typeof resp.coberturas[0] != "undefined" &&
                typeof resp.coberturas[0].amplia != "undefined"
              ) {
                resp.coberturas = resp.coberturas[0].amplia;
              } else if (
                typeof resp.coberturas != "undefined" &&
                typeof resp.coberturas[1] != "undefined" &&
                typeof resp.coberturas[1].amplia != "undefined"
              ) {
                resp.coberturas = resp.coberturas[1].amplia;
              } else if (
                typeof resp.coberturas != "undefined" &&
                typeof resp.coberturas[2] != "undefined" &&
                typeof resp.coberturas[2].amplia != "undefined"
              ) {
                resp.coberturas = resp.coberturas[2].amplia;
              }
              if (
                typeof resp.cotizacion != "undefined" &&
                typeof resp.cotizacion[0] != "undefined" &&
                typeof resp.cotizacion[0].amplia != "undefined" &&
                typeof resp.cotizacion[0].amplia[0] != "undefined" &&
                typeof resp.cotizacion[0].amplia[0].anual != "undefined"
              ) {
                resp.cotizacion = resp.cotizacion[0].amplia[0].anual;
              } else if (
                typeof resp.cotizacion != "undefined" &&
                typeof resp.cotizacion[1] != "undefined" &&
                typeof resp.cotizacion[1].amplia != "undefined" &&
                typeof resp.cotizacion[1].amplia[0] != "undefined" &&
                typeof resp.cotizacion[1].amplia[0].anual != "undefined"
              ) {
                resp.cotizacion = resp.cotizacion[1].amplia[0].anual;
              } else if (
                typeof resp.cotizacion != "undefined" &&
                typeof resp.cotizacion[2] != "undefined" &&
                typeof resp.cotizacion[2].amplia != "undefined" &&
                typeof resp.cotizacion[2].amplia[0] != "undefined" &&
                typeof resp.cotizacion[2].amplia[0].anual != "undefined"
              ) {
                resp.cotizacion = resp.cotizacion[2].amplia[0].anual;
              }
              if (typeof resp != "string") {
                if (
                  Object.keys(resp.cotizacion).includes("primaTotal") &&
                  resp.cotizacion.primaTotal != null &&
                  resp.cotizacion.primaTotal != ""
                ) {
                  state.config.precio = state.cotizacion.cotizacion.primaTotal;
                  this.commit("valid");
                  this.commit("cotizacionesAli");
                  this.commit("limpiarCoberturas");
                } else {
                  this.commit("cotizacionesAli");
                }
              } else {
                this.commit("cotizacionesAli");
              }
            }
            // FIN VALIDACION MIGO
            else {
              state.cotizacion = resp;
              // RESPUESTA PARA TODAS LAS ASEGURADORAS
              if (typeof resp != "string") {
                //IF-PRECIO

                if (
                  Object.keys(resp.cotizacion).includes("primaTotal") &&
                  resp.cotizacion.primaTotal != null &&
                  resp.cotizacion.primaTotal != ""
                ) {
                  state.cargandocotizacion = true;
                  state.config.precio = state.cotizacion.cotizacion.primaTotal;
                  this.commit("cotizacionesAli");
                  this.commit("limpiarCoberturas");
                } //FIN DEL IF-PRECIO
                else {
                  console.log(
                    state.config.aseguradora + " no trajo precios D:"
                  );
                  this.commit("cotizacionesAli");
                }
              } //IF resp - no vacio
              else {
                this.commit("cotizacionesAli");
                console.log(
                  "Problema al cotizar con " + state.config.aseguradora
                );
                state.config.loading = false;
              }
              // FIN RESPUESTA PARA TODAS LAS ASEGURADORAS
            }
          })
          .catch((error) => {
            state.status = error.status
              ? error.status
              : error.response
              ? error.response.status
              : 500;
            if (state.status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("cotizarInterno");
                })
                .catch((error) => {
                  state.status = error.status
                    ? error.status
                    : error.response
                    ? error.response.status
                    : 500;
                  state.idEndPoint = 383;
                  state.message = error.data
                    ? JSON.stringify({
                        error: error.data,
                        peticion: { tokenData: dbService.tokenData },
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                  this.commit("cotizacionesAli");
                });
            } else {
              state.idEndPoint = 284;
              state.message = error.data
                ? JSON.stringify({
                    error: error.data,
                    peticion: data,
                  })
                : JSON.stringify({
                    error: error,
                    peticion: data,
                  });

              state.statusCot = true;
              // debugger
              state.cotizacion = error.response.data.data;

              // console.log(error.response.data.data,'aqui esta el error')
              // this.commit("cotizacionesAli");
              this.commit("valid");
            }
            this.commit("cotizacionesAli");
          });
      },
      limpiarCoberturas(state) {
        if (state.cotizacion.Coberturas) {
          for (var c in state.cotizacion.Coberturas[0]) {
            if (
              state.cotizacion.Coberturas[0][c] === state.limpiaCoberturas ||
              state.cotizacion.Coberturas[0][c] === "" ||
              state.cotizacion.Coberturas[0][c] === "-"
            ) {
              delete state.cotizacion.Coberturas[0][c];
            }
          }
        }
      },
      v1cotizaciones: function (state) {
        //console.log('Guardando Prospecto');
        //console.log(state.formData.nombre);
        //state.formData.aseguradora = 'MIITUO';

        let datav1cotizaciones = {
          nombreUsr: state.formData.nombre,
          emailUsr: state.formData.correo,
          datosCot: JSON.stringify(state.formData),
          telefonoUsr: state.formData.telefono,
          generoUsr: state.formData.genero,
          codigoPostalUsr: state.formData.cp,
          edadUsr: state.formData.edad,
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          ip: state.config.ipCliente,
        };
        newProspectNewCoreService
          .nuevoProspecto(
            JSON.stringify(datav1cotizaciones),
            state.config.accessToken
          )
          .then((resp) => {
            /*NUMERO DE COTIZACION DEL CLIENTE
             * ID-COTIZACION*/
            state.config.idCotizacionNewCore = resp.data;
            this.commit("cotizacionesAli");
          })
          .catch((error) => {
            state.status = error.status
              ? error.status
              : error.response
              ? error.response.status
              : 500;
            if (state.status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("v1cotizaciones");
                })
                .catch((error) => {
                  state.status = error.status
                    ? error.status
                    : error.response
                    ? error.response.status
                    : 500;
                  state.idEndPoint = 383;
                  state.message = erro.data
                    ? JSON.stringify({
                        error: error.data,
                        peticion: { tokenData: dbService.tokenData },
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                });
            } else {
              state.idEndPoint = 231;
              state.message = error.data
                ? JSON.stringify({
                    error: error.data,
                    peticion: datav1cotizaciones,
                  })
                : JSON.stringify({
                    error: error,
                    peticion: datav1cotizaciones,
                  });
            }
          });
      },
      sendLeadMiituo: function (state) {
        let dataLeads = {
          idSubRamo: state.config.idSubRamo,
          idLeadSocio: 123456,
          idCotizacionAli: state.config.id_cotizacion_a.data,
          estado: 1,
          numPoliza: "",
          referencia: "{}",
        };
        wsLeadVendido
          .nuevoLeadVendido(JSON.stringify(dataLeads))
          .then((resp) => {
            console.log("LEAD VENDIDO MIITUO");
          })
          .catch((error) => {
            state.status = error.status
              ? error.status
              : error.response
              ? error.response.status
              : 500;
            if (state.status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("sendLeadMiituo");
                })
                .catch((error) => {
                  state.status = error.status
                    ? error.status
                    : error.response
                    ? error.response.status
                    : 500;
                  state.idEndPoint = 383;
                  state.message = error.data
                    ? JSON.stringify({
                        error: error.data,
                        peticion: { tokenData: dbService.tokenData },
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                });
            } else {
              state.idEndPoint = 281;
              state.message = error.data
                ? JSON.stringify({
                    error: error.response.data,
                    peticion: dataLeads,
                  })
                : JSON.stringify({
                    error: error,
                    peticion: dataLeads,
                  });
            }
          });
      },
      saveProspecto: function (state) {
        state.formData.dominioCorreo = state.formData.dominioCorreo;
        let dataProspecto = {
          nombreUsr: state.formData.nombre,
          emailUsr: state.formData.correo,
          datosCot: JSON.stringify(state.formData),
          telefonoUsr: state.formData.telefono,
          generoUsr: state.formData.genero,
          codigoPostalUsr: state.formData.cp,
          edadUsr: state.formData.edad,
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          ip: state.config.ipCliente,
          idSubRamo: state.config.idSubRamo,
          peticionAli: JSON.stringify(state.config.dataPeticionCotizacion),
        };

        cotizacionBranding
          .newCotizacion(
            JSON.stringify(dataProspecto),
            state.config.accessToken
          )
          .then((resp) => {
            /*NUMERO DE COTIZACION DEL CLIENTE
             * idCotizaciones
             * idProductoSolicitud
             * */
            //

            state.config.idCotizacionNewCore = resp.data.data.idCotizaciones;
            state.config.idProductoSolicitud =
              resp.data.data.idProductoSolicitud;
            state.config.cotizacionAli = resp.data.data.idCotizacionAli;

            // debugger
            if (state.config.cotizacion) {
              if (state.cotizacionInterna) {
                this.commit("cotizarInterno");
              }
            } else {
              this.commit("cotizacionesAli");
            }
          })
          .catch((error) => {
            state.status = error.status
              ? error.status
              : error.response
              ? error.response.status
              : 500;
            if (state.status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("saveProspecto");
                })
                .catch((error) => {
                  state.status = error.status
                    ? error.status
                    : error.response
                    ? error.response.status
                    : 500;
                  state.idEndPoint = 383;
                  state.message = error.data
                    ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data),
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                  this.commit("valid");
                });
            } else {
              this.commit("valid");
              state.idEndPoint = 491;
              state.message = error.data
                ? JSON.stringify({
                    error: error.response.data,
                    peticion: JSON.parse(error.config.data),
                  })
                : JSON.stringify({ error: error, peticion: dataProspecto });
            }
          });
      },
      cotizacionesAli: function (state) {
        var peticionAli =
          state.config.ventaLead == true
            ? JSON.stringify(state.formData)
            : JSON.stringify(state.config.dataPeticionCotizacion);

        var jsonPeticionWs = {
          idCotizacion: state.config.idCotizacionNewCore,
          idSubRamo: state.config.idSubRamo,
          peticion: peticionAli,
          respuesta: state.statusCot
            ? state.cotizacion
            : JSON.stringify(state.cotizacion), //POR ESTRETEGIA AL TARDAR MUCHO EN LA COTIZACION DE GNP
        };
        // console.log(state.cotizacion)
        cotizacionAliService
          .nuevaCotizacion(
            JSON.stringify(jsonPeticionWs),
            state.config.accessToken,
            state.config.cotizacionAli,
            state.config.dataAli
          )
          .then((respNuevaCotizacion) => {
            state.config.id_cotizacion_a = respNuevaCotizacion;
            if (state.config.ventaLead == true) {
              this.commit("sendLeadMiituo");
            }

            this.commit("valid");
          })
          .catch((error) => {
            console.log(error);
            state.status = error.status
              ? error.status
              : error.response
              ? error.response.status
              : 500;
            if (state.status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("cotizacionesAli");
                })
                .catch((error) => {
                  state.status = error.status
                    ? error.status
                    : error.response
                    ? error.response.status
                    : 500;
                  state.idEndPoint = 383;
                  state.message = error.data
                    ? JSON.stringify({
                        error: error.data,
                        peticion: { tokenData: dbService.tokenData },
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                  this.commit("valid");
                });
            } else {
              state.idEndPoint = 227;
              state.message = error.data
                ? JSON.stringify({
                    error: error.data,
                    peticion: jsonPeticionWs,
                  })
                : JSON.stringify({
                    error: error,
                    peticion: jsonPeticionWs,
                  });
              this.commit("valid");
            }
            this.commit("valid");
          });
      },
      valid: function (state) {
        // debugger
        //console.log("Validando datos de la cotizacion");
        if (state.config.precio === "" && state.ejecutivo.id === "") {
          state.msj = true;
        }
        if (
          state.config.precio === 0.0 ||
          state.config.precio === "" ||
          state.config.precio < 1000 ||
          !state.config.precio
        ) {
          state.msjEje = true;
          state.config.loading = false;
        } else {
          state.cargandocotizacion = true;
        }
      },
    },
  });
};
export default createStore;
